
package ee.ttu.idu0075._2015.ws.carrepair;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ee.ttu.idu0075._2015.ws.carrepair package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AddCarRepairCarPartResponse_QNAME = new QName("http://www.ttu.ee/idu0075/2015/ws/carRepair", "addCarRepairCarPartResponse");
    private final static QName _AddCarPartResponse_QNAME = new QName("http://www.ttu.ee/idu0075/2015/ws/carRepair", "addCarPartResponse");
    private final static QName _GetCarRepairResponse_QNAME = new QName("http://www.ttu.ee/idu0075/2015/ws/carRepair", "getCarRepairResponse");
    private final static QName _GetCarRepairCarPartListResponse_QNAME = new QName("http://www.ttu.ee/idu0075/2015/ws/carRepair", "getCarRepairCarPartListResponse");
    private final static QName _GetCarPartResponse_QNAME = new QName("http://www.ttu.ee/idu0075/2015/ws/carRepair", "getCarPartResponse");
    private final static QName _AddCarRepairResponse_QNAME = new QName("http://www.ttu.ee/idu0075/2015/ws/carRepair", "addCarRepairResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ee.ttu.idu0075._2015.ws.carrepair
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetCarRepairCarPartListRequest }
     * 
     */
    public GetCarRepairCarPartListRequest createGetCarRepairCarPartListRequest() {
        return new GetCarRepairCarPartListRequest();
    }

    /**
     * Create an instance of {@link CarPartType }
     * 
     */
    public CarPartType createCarPartType() {
        return new CarPartType();
    }

    /**
     * Create an instance of {@link AddCarPartRequest }
     * 
     */
    public AddCarPartRequest createAddCarPartRequest() {
        return new AddCarPartRequest();
    }

    /**
     * Create an instance of {@link GetCarPartListResponse }
     * 
     */
    public GetCarPartListResponse createGetCarPartListResponse() {
        return new GetCarPartListResponse();
    }

    /**
     * Create an instance of {@link AddCarRepairRequest }
     * 
     */
    public AddCarRepairRequest createAddCarRepairRequest() {
        return new AddCarRepairRequest();
    }

    /**
     * Create an instance of {@link AddCarRepairCarPartRequest }
     * 
     */
    public AddCarRepairCarPartRequest createAddCarRepairCarPartRequest() {
        return new AddCarRepairCarPartRequest();
    }

    /**
     * Create an instance of {@link CarRepairType }
     * 
     */
    public CarRepairType createCarRepairType() {
        return new CarRepairType();
    }

    /**
     * Create an instance of {@link GetCarPartRequest }
     * 
     */
    public GetCarPartRequest createGetCarPartRequest() {
        return new GetCarPartRequest();
    }

    /**
     * Create an instance of {@link GetCarRepairListResponse }
     * 
     */
    public GetCarRepairListResponse createGetCarRepairListResponse() {
        return new GetCarRepairListResponse();
    }

    /**
     * Create an instance of {@link CarRepairCarPartType }
     * 
     */
    public CarRepairCarPartType createCarRepairCarPartType() {
        return new CarRepairCarPartType();
    }

    /**
     * Create an instance of {@link GetCarRepairRequest }
     * 
     */
    public GetCarRepairRequest createGetCarRepairRequest() {
        return new GetCarRepairRequest();
    }

    /**
     * Create an instance of {@link GetCarRepairListRequest }
     * 
     */
    public GetCarRepairListRequest createGetCarRepairListRequest() {
        return new GetCarRepairListRequest();
    }

    /**
     * Create an instance of {@link CarRepairCarPartListType }
     * 
     */
    public CarRepairCarPartListType createCarRepairCarPartListType() {
        return new CarRepairCarPartListType();
    }

    /**
     * Create an instance of {@link GetCarPartListRequest }
     * 
     */
    public GetCarPartListRequest createGetCarPartListRequest() {
        return new GetCarPartListRequest();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CarRepairCarPartType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/2015/ws/carRepair", name = "addCarRepairCarPartResponse")
    public JAXBElement<CarRepairCarPartType> createAddCarRepairCarPartResponse(CarRepairCarPartType value) {
        return new JAXBElement<CarRepairCarPartType>(_AddCarRepairCarPartResponse_QNAME, CarRepairCarPartType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CarPartType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/2015/ws/carRepair", name = "addCarPartResponse")
    public JAXBElement<CarPartType> createAddCarPartResponse(CarPartType value) {
        return new JAXBElement<CarPartType>(_AddCarPartResponse_QNAME, CarPartType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CarRepairType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/2015/ws/carRepair", name = "getCarRepairResponse")
    public JAXBElement<CarRepairType> createGetCarRepairResponse(CarRepairType value) {
        return new JAXBElement<CarRepairType>(_GetCarRepairResponse_QNAME, CarRepairType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CarRepairCarPartListType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/2015/ws/carRepair", name = "getCarRepairCarPartListResponse")
    public JAXBElement<CarRepairCarPartListType> createGetCarRepairCarPartListResponse(CarRepairCarPartListType value) {
        return new JAXBElement<CarRepairCarPartListType>(_GetCarRepairCarPartListResponse_QNAME, CarRepairCarPartListType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CarPartType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/2015/ws/carRepair", name = "getCarPartResponse")
    public JAXBElement<CarPartType> createGetCarPartResponse(CarPartType value) {
        return new JAXBElement<CarPartType>(_GetCarPartResponse_QNAME, CarPartType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CarRepairType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/2015/ws/carRepair", name = "addCarRepairResponse")
    public JAXBElement<CarRepairType> createAddCarRepairResponse(CarRepairType value) {
        return new JAXBElement<CarRepairType>(_AddCarRepairResponse_QNAME, CarRepairType.class, null, value);
    }

}
