/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carrepairui;

import ee.ttu.idu0075._2015.ws.carrepair.AddCarPartRequest;
import ee.ttu.idu0075._2015.ws.carrepair.CarPartType;
import ee.ttu.idu0075._2015.ws.carrepair.GetCarPartRequest;
import java.math.BigInteger;

/**
 *
 * @author karlkristjankalluste
 */
public class CarRepairUI {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       AddCarPartRequest addCarPart = new AddCarPartRequest();
       addCarPart.setBrand("mustang");
       addCarPart.setCode("AGH54");
       addCarPart.setColor("yellow");
       addCarPart.setManufactureYear(BigInteger.valueOf(2003));
       addCarPart.setManufacturer("generalMotors");
       addCarPart.setName("mirror");
       addCarPart.setToken("salajane");
       
       CarPartType carPart = addCarPart(addCarPart);
       
       GetCarPartRequest getCarPart = new GetCarPartRequest();
       getCarPart.setToken("salajane");
       getCarPart.setId(carPart.getId());
       
       System.out.println(carPart.getName() + ' ' + carPart.getColor());
    }
    
    

    private static CarPartType addCarPart(ee.ttu.idu0075._2015.ws.carrepair.AddCarPartRequest parameter) {
        ee.ttu.idu0075._2015.ws.carrepair.CarRepairService service = new ee.ttu.idu0075._2015.ws.carrepair.CarRepairService();
        ee.ttu.idu0075._2015.ws.carrepair.CarRepairPortType port = service.getCarRepairPort();
        return port.addCarPart(parameter);
    }

    private static CarPartType getCarPart(ee.ttu.idu0075._2015.ws.carrepair.GetCarPartRequest parameter) {
        ee.ttu.idu0075._2015.ws.carrepair.CarRepairService service = new ee.ttu.idu0075._2015.ws.carrepair.CarRepairService();
        ee.ttu.idu0075._2015.ws.carrepair.CarRepairPortType port = service.getCarRepairPort();
        return port.getCarPart(parameter);
    }
    
}
