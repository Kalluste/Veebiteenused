
package ee.ttu.idu0075._2015.ws.carrepair;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for carRepairType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="carRepairType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="createdDate" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="location" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="openTime" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="closeTime" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="phoneNumber" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="carRepairCarPartList" type="{http://www.ttu.ee/idu0075/2015/ws/carRepair}carRepairCarPartListType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "carRepairType", propOrder = {
    "id",
    "createdDate",
    "location",
    "name",
    "openTime",
    "closeTime",
    "phoneNumber",
    "carRepairCarPartList"
})
public class CarRepairType {

    @XmlElement(required = true)
    protected BigInteger id;
    @XmlElement(required = true)
    protected BigInteger createdDate;
    @XmlElement(required = true)
    protected String location;
    @XmlElement(required = true)
    protected String name;
    protected double openTime;
    protected double closeTime;
    protected BigInteger phoneNumber;
    @XmlElement(required = true)
    protected CarRepairCarPartListType carRepairCarPartList;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setId(BigInteger value) {
        this.id = value;
    }

    /**
     * Gets the value of the createdDate property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCreatedDate() {
        return createdDate;
    }

    /**
     * Sets the value of the createdDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCreatedDate(BigInteger value) {
        this.createdDate = value;
    }

    /**
     * Gets the value of the location property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocation() {
        return location;
    }

    /**
     * Sets the value of the location property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocation(String value) {
        this.location = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the openTime property.
     * 
     */
    public double getOpenTime() {
        return openTime;
    }

    /**
     * Sets the value of the openTime property.
     * 
     */
    public void setOpenTime(double value) {
        this.openTime = value;
    }

    /**
     * Gets the value of the closeTime property.
     * 
     */
    public double getCloseTime() {
        return closeTime;
    }

    /**
     * Sets the value of the closeTime property.
     * 
     */
    public void setCloseTime(double value) {
        this.closeTime = value;
    }

    /**
     * Gets the value of the phoneNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Sets the value of the phoneNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPhoneNumber(BigInteger value) {
        this.phoneNumber = value;
    }

    /**
     * Gets the value of the carRepairCarPartList property.
     * 
     * @return
     *     possible object is
     *     {@link CarRepairCarPartListType }
     *     
     */
    public CarRepairCarPartListType getCarRepairCarPartList() {
        return carRepairCarPartList;
    }

    /**
     * Sets the value of the carRepairCarPartList property.
     * 
     * @param value
     *     allowed object is
     *     {@link CarRepairCarPartListType }
     *     
     */
    public void setCarRepairCarPartList(CarRepairCarPartListType value) {
        this.carRepairCarPartList = value;
    }

}
