
package ee.ttu.idu0075._2015.ws.carrepair;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.11-b150120.1832
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "CarRepairService", targetNamespace = "http://www.ttu.ee/idu0075/2015/ws/carRepair", wsdlLocation = "file:/Users/karlkristjankalluste/NetBeansProjects/CarRepairWebApplication/src/conf/xml-resources/web-services/CarRepairWebServiceFromWSDL/wsdl/Autoparandus.wsdl")
public class CarRepairService
    extends Service
{

    private final static URL CARREPAIRSERVICE_WSDL_LOCATION;
    private final static WebServiceException CARREPAIRSERVICE_EXCEPTION;
    private final static QName CARREPAIRSERVICE_QNAME = new QName("http://www.ttu.ee/idu0075/2015/ws/carRepair", "CarRepairService");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("file:/Users/karlkristjankalluste/NetBeansProjects/CarRepairWebApplication/src/conf/xml-resources/web-services/CarRepairWebServiceFromWSDL/wsdl/Autoparandus.wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        CARREPAIRSERVICE_WSDL_LOCATION = url;
        CARREPAIRSERVICE_EXCEPTION = e;
    }

    public CarRepairService() {
        super(__getWsdlLocation(), CARREPAIRSERVICE_QNAME);
    }

    public CarRepairService(WebServiceFeature... features) {
        super(__getWsdlLocation(), CARREPAIRSERVICE_QNAME, features);
    }

    public CarRepairService(URL wsdlLocation) {
        super(wsdlLocation, CARREPAIRSERVICE_QNAME);
    }

    public CarRepairService(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, CARREPAIRSERVICE_QNAME, features);
    }

    public CarRepairService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public CarRepairService(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns CarRepairPortType
     */
    @WebEndpoint(name = "CarRepairPort")
    public CarRepairPortType getCarRepairPort() {
        return super.getPort(new QName("http://www.ttu.ee/idu0075/2015/ws/carRepair", "CarRepairPort"), CarRepairPortType.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns CarRepairPortType
     */
    @WebEndpoint(name = "CarRepairPort")
    public CarRepairPortType getCarRepairPort(WebServiceFeature... features) {
        return super.getPort(new QName("http://www.ttu.ee/idu0075/2015/ws/carRepair", "CarRepairPort"), CarRepairPortType.class, features);
    }

    private static URL __getWsdlLocation() {
        if (CARREPAIRSERVICE_EXCEPTION!= null) {
            throw CARREPAIRSERVICE_EXCEPTION;
        }
        return CARREPAIRSERVICE_WSDL_LOCATION;
    }

}
