/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.ttu.idu0075.carrepair;

import ee.ttu.idu0075._2015.ws.carrepair.CarPartType;
import ee.ttu.idu0075._2015.ws.carrepair.CarRepairCarPartListType;
import ee.ttu.idu0075._2015.ws.carrepair.CarRepairCarPartType;
import ee.ttu.idu0075._2015.ws.carrepair.CarRepairType;
import ee.ttu.idu0075._2015.ws.carrepair.GetCarPartListResponse;
import ee.ttu.idu0075._2015.ws.carrepair.GetCarRepairListResponse;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.jws.WebService;

/**
 *
 * @author karlkristjankalluste
 */
@WebService(serviceName = "CarRepairService", portName = "CarRepairPort", endpointInterface = "ee.ttu.idu0075._2015.ws.carrepair.CarRepairPortType", targetNamespace = "http://www.ttu.ee/idu0075/2015/ws/carRepair", wsdlLocation = "WEB-INF/wsdl/CarRepairWebServiceFromWSDL/Autoparandus.wsdl")
public class CarRepairService {
    public static int id = 0;
    public static final List<CarRepairType> carRepairList = new ArrayList<>();
    public static final List<CarPartType> carPartList = new ArrayList<>();
    public static final List<BigInteger> carRepairRequestCodeList = new ArrayList<>();
    
    public CarRepairService() {
      
       
       CarPartType carPart = new CarPartType();
       carPart.setBrand("Volvo");
       carPart.setCode("AFG2");
       carPart.setColor("yellow");
       carPart.setId(BigInteger.valueOf(id++));
       carPart.setManufacturer("Volvo");
       carPart.setManufactureYear(BigInteger.valueOf(2008));
       carPart.setName("rear-mirror");
       carPartList.add(carPart);
       
       CarPartType carPart2 = new CarPartType();
       carPart2.setBrand("Saab");
       carPart2.setCode("AFH2");
       carPart2.setColor("Brown");
       carPart2.setId(BigInteger.valueOf(id++));
       carPart2.setManufacturer("Mustang");
       carPart2.setManufactureYear(BigInteger.valueOf(2004));
       carPart2.setName("Lantern");
       carPartList.add(carPart2);
       
       CarRepairCarPartType carRepairCarPart = new CarRepairCarPartType();
       carRepairCarPart.setCarPart(carPart);
       carRepairCarPart.setQuantity(BigInteger.ONE);
       carRepairCarPart.setUnitPrice(BigInteger.valueOf(164));
       
       CarRepairCarPartListType carRepairCarPartListType = new CarRepairCarPartListType();
       carRepairCarPartListType.getCarRepairCarPart().add(carRepairCarPart);
   
       CarRepairType carRepair = new CarRepairType();
       carRepair.setId(BigInteger.valueOf(id++));
       carRepair.setName("puurmann");
       carRepair.setCreatedDate(BigInteger.valueOf(2003));
       carRepair.setCloseTime(20);
       carRepair.setOpenTime(10);
       carRepair.setLocation("Tallinn, Mustamägi");
       carRepair.setPhoneNumber(BigInteger.valueOf(53583751));
       carRepair.setCarRepairCarPartList(carRepairCarPartListType);
       carRepairList.add(carRepair);
    }
    
    public ee.ttu.idu0075._2015.ws.carrepair.CarPartType getCarPart(ee.ttu.idu0075._2015.ws.carrepair.GetCarPartRequest parameter) {
        if(isValidToken(parameter.getToken())){
            for(CarPartType carPart: carPartList){
                if(carPart.getId().equals(parameter.getId())){
                    return carPart;
                }
            }
            throw new RuntimeException("CarPart not found!");
        }
        throw new RuntimeException("Invalid token!");
    }

    public ee.ttu.idu0075._2015.ws.carrepair.CarPartType addCarPart(ee.ttu.idu0075._2015.ws.carrepair.AddCarPartRequest parameter) {
        if(isValidToken(parameter.getToken())){
           for(CarPartType carPart : carPartList){
               if(carPart.getCode().equals(parameter.getCode())){
                   throw new RuntimeException("carPart already existing!");
               }
           }
           CarPartType carPart = new CarPartType();
           carPart.setBrand(parameter.getBrand());
           carPart.setCode(parameter.getCode());
           carPart.setColor(parameter.getColor());
           carPart.setId(BigInteger.valueOf(id++));
           carPart.setManufacturer(parameter.getManufacturer());
           carPart.setManufactureYear(parameter.getManufactureYear());
           carPart.setName(parameter.getName());
           carPartList.add(carPart);
           return carPart;
        }
        throw new RuntimeException("Invalid token!");
    }

    public ee.ttu.idu0075._2015.ws.carrepair.GetCarPartListResponse getCarPartList(ee.ttu.idu0075._2015.ws.carrepair.GetCarPartListRequest parameter) {
        if(isValidToken(parameter.getToken())){
            GetCarPartListResponse getCarPartListResponse = new GetCarPartListResponse();
            getCarPartListResponse.getCarPart().addAll(carPartList);
            return getCarPartListResponse;
        }
        throw new RuntimeException("Invalid token!");
    }

    public ee.ttu.idu0075._2015.ws.carrepair.CarRepairType getCarRepair(ee.ttu.idu0075._2015.ws.carrepair.GetCarRepairRequest parameter) {
        if(isValidToken(parameter.getToken())){
            for(CarRepairType carRepair: carRepairList){
                if(carRepair.getId().equals(parameter.getId())){
                    return carRepair;
                }
            }
            throw new RuntimeException("CarRepair not found!");
        }
        throw new RuntimeException("Invalid token!");
    }

    public ee.ttu.idu0075._2015.ws.carrepair.CarRepairType addCarRepair(ee.ttu.idu0075._2015.ws.carrepair.AddCarRepairRequest parameter) {
        
        if(isValidToken(parameter.getToken())){
           for(BigInteger code: carRepairRequestCodeList){
               if(parameter.getRequestCode().equals(code)){
                   throw new RuntimeException("carRepair already existing!");
               }
           }
           carRepairRequestCodeList.add(parameter.getRequestCode());
           CarRepairType carRepair = new CarRepairType();
           carRepair.setCloseTime(parameter.getCloseTime());
           carRepair.setCreatedDate(parameter.getCreatedDate());
           carRepair.setId(BigInteger.valueOf(id++));
           carRepair.setLocation(parameter.getLocation());
           carRepair.setName(parameter.getName());
           carRepair.setOpenTime(parameter.getOpenTime());
           carRepair.setPhoneNumber(parameter.getPhoneNumber());
           carRepair.setCarRepairCarPartList(new CarRepairCarPartListType());
           carRepairList.add(carRepair);
           return carRepair;
        }
        throw new RuntimeException("Invalid token!");
    }

    public ee.ttu.idu0075._2015.ws.carrepair.GetCarRepairListResponse getCarRepairList(ee.ttu.idu0075._2015.ws.carrepair.GetCarRepairListRequest parameter) {
        if(isValidToken(parameter.getToken())){
           List<CarRepairType> carRepairListValid = new ArrayList<>();
           for(CarRepairType carRepair: carRepairList){
               if(parameter.getCurrentlyOpen() != null) {
                   if (parameter.getCurrentlyOpen() < carRepair.getOpenTime() ||
                        parameter.getCurrentlyOpen() > carRepair.getCloseTime()) {
                           continue;
                    }
               }
               if(parameter.getHasRelatedCarParts() != null){
                   if(!parameter.getHasRelatedCarParts().equals("yes") || carRepair.getCarRepairCarPartList().getCarRepairCarPart().isEmpty()){
                       continue;
                   }
               }
               if(parameter.getLocation() != null){
                   if(!parameter.getLocation().equals(carRepair.getLocation())){
                       continue;
                   }
               }
               carRepairListValid.add(carRepair);
           }
           GetCarRepairListResponse response = new GetCarRepairListResponse();
           response.getCarRepair().addAll(carRepairListValid);
           return response;
        }
        throw new RuntimeException("Invalid token!");
    }

    public ee.ttu.idu0075._2015.ws.carrepair.CarRepairCarPartListType getCarRepairCarPartList(ee.ttu.idu0075._2015.ws.carrepair.GetCarRepairCarPartListRequest parameter) {
        if(isValidToken(parameter.getToken())){
            for(CarRepairType carRepair: carRepairList){
                if(carRepair.getId().equals(parameter.getCarRepairId())){
                    return carRepair.getCarRepairCarPartList();
                }
            }
            throw new RuntimeException("Did not find shop");
        }
        throw new RuntimeException("Invalid token!");
    }

    public ee.ttu.idu0075._2015.ws.carrepair.CarRepairCarPartType addCarRepairCarPart(ee.ttu.idu0075._2015.ws.carrepair.AddCarRepairCarPartRequest parameter) {
        if(isValidToken(parameter.getToken())){
            for(CarRepairType carRepair: carRepairList){
                if(carRepair.getId().equals(parameter.getCarRepairId())) {
                    for(CarPartType carPart : carPartList){
                        if(carPart.getId().equals(parameter.getCarPartId())){
                            CarRepairCarPartType carRepairCarPartType = new CarRepairCarPartType();
                            carRepairCarPartType.setQuantity(parameter.getQuantity());
                            carRepairCarPartType.setUnitPrice(parameter.getUnitPrice());
                            carRepairCarPartType.setCarPart(carPart);
                            carRepair.getCarRepairCarPartList().getCarRepairCarPart().add(carRepairCarPartType);
                            return carRepairCarPartType;
                        }
                    }
                    throw new RuntimeException("Did not find carPartType!");
                }
            }
            throw new RuntimeException("Did not find shop");
        }
        throw new RuntimeException("Invalid token!");
    }
    
    public static final boolean isValidToken(String token){
        return "salajane".equals(token);
    }
    public static final List<CarRepairType> getCarRepairList(){
        return carRepairList;
    }
    public static final List<CarPartType> getCarPartList(){
        return carPartList;
    }
    
}
