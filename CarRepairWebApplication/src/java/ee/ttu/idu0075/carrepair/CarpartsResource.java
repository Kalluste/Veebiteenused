/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.ttu.idu0075.carrepair;

import ee.ttu.idu0075._2015.ws.carrepair.AddCarPartRequest;
import ee.ttu.idu0075._2015.ws.carrepair.CarPartType;
import ee.ttu.idu0075._2015.ws.carrepair.GetCarPartListRequest;
import ee.ttu.idu0075._2015.ws.carrepair.GetCarPartListResponse;
import ee.ttu.idu0075._2015.ws.carrepair.GetCarPartRequest;
import static ee.ttu.idu0075.carrepair.CarRepairService.carPartList;
import static ee.ttu.idu0075.carrepair.CarRepairService.isValidToken;
import java.math.BigInteger;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author karlkristjankalluste
 */
@Path("carparts")
public class CarpartsResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of CarpartsResource
     */
    private CarRepairService service;
    public CarpartsResource() {
        this.service = new CarRepairService();
    }
    
    @GET
    @Path("{id : \\d+}")
    @Produces(MediaType.APPLICATION_JSON)
    public CarPartType getCarPart(
            @PathParam("id") BigInteger id,
            @QueryParam("token") String token){
            GetCarPartRequest request = new GetCarPartRequest();
            request.setId(id);
            request.setToken(token);
            
            return service.getCarPart(request);
    }
    
    @PUT
    @Consumes("application/json")
    @Produces(MediaType.APPLICATION_JSON)
    public CarPartType addCarPart(
            @QueryParam("token") String token,
            AddCarPartRequest content
            ){
            content.setToken(token);
            return service.addCarPart(content);
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public GetCarPartListResponse getCarPartList(
            @QueryParam("token") String token){
            GetCarPartListRequest request = new GetCarPartListRequest();
            request.setToken(token);
            
            return service.getCarPartList(request);
    }
}
