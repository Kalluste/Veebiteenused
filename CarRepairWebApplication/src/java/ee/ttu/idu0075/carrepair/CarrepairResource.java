/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.ttu.idu0075.carrepair;

import ee.ttu.idu0075._2015.ws.carrepair.AddCarPartRequest;
import ee.ttu.idu0075._2015.ws.carrepair.AddCarRepairCarPartRequest;
import ee.ttu.idu0075._2015.ws.carrepair.AddCarRepairRequest;
import ee.ttu.idu0075._2015.ws.carrepair.CarPartType;
import ee.ttu.idu0075._2015.ws.carrepair.CarRepairCarPartListType;
import ee.ttu.idu0075._2015.ws.carrepair.CarRepairCarPartType;
import ee.ttu.idu0075._2015.ws.carrepair.CarRepairType;
import ee.ttu.idu0075._2015.ws.carrepair.GetCarPartListRequest;
import ee.ttu.idu0075._2015.ws.carrepair.GetCarPartListResponse;
import ee.ttu.idu0075._2015.ws.carrepair.GetCarPartRequest;
import ee.ttu.idu0075._2015.ws.carrepair.GetCarRepairCarPartListRequest;
import ee.ttu.idu0075._2015.ws.carrepair.GetCarRepairListRequest;
import ee.ttu.idu0075._2015.ws.carrepair.GetCarRepairListResponse;
import ee.ttu.idu0075._2015.ws.carrepair.GetCarRepairRequest;
import java.math.BigInteger;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author karlkristjankalluste
 */
@Path("carrepair")
public class CarrepairResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of CarrepairResource
     */
    private CarRepairService service;

    public CarrepairResource() {
        this.service = new CarRepairService();
    }
    
    @GET
    @Path("{id : \\d+}")
    @Produces(MediaType.APPLICATION_JSON)
    public CarRepairType getCarRepair(
            @PathParam("id") BigInteger id,
            @QueryParam("token") String token){
            GetCarRepairRequest request = new GetCarRepairRequest();
            request.setId(id);
            request.setToken(token);
            
            return service.getCarRepair(request);
    }
    @PUT
    @Consumes("application/json")
    @Produces(MediaType.APPLICATION_JSON)
    public CarRepairType addCarRepair(
            @QueryParam("token") String token,
            AddCarRepairRequest content
            ){
            content.setToken(token);
            return service.addCarRepair(content);
    }
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public GetCarRepairListResponse getCarRepairList(
            @QueryParam("token") String token,
            @QueryParam("currentlyOpen") double currentlyOpen,
            @QueryParam("hasRelatedCarParts") String hasRelatedCarParts,
            @QueryParam("location") String location
            
    ){
            GetCarRepairListRequest request = new GetCarRepairListRequest();
            request.setToken(token);
            
            return service.getCarRepairList(request);
    }
    
    @GET
    @Path("/list/{id : \\d+}")
    @Produces(MediaType.APPLICATION_JSON)
    public CarRepairCarPartListType getCarRepairCarPartList(
            @PathParam("id") BigInteger id,
            @QueryParam("token") String token
            ){
            GetCarRepairCarPartListRequest request = new GetCarRepairCarPartListRequest();
            request.setCarRepairId(id);
            request.setToken(token);
            
            return service.getCarRepairCarPartList(request);
    }
    
    @PUT
    @Path("carpart")
    @Consumes("application/json")
    @Produces(MediaType.APPLICATION_JSON)
    public CarRepairCarPartType addCarRepairCarPart(
            @QueryParam("token") String token,
            AddCarRepairCarPartRequest request){
            if (request.getQuantity().compareTo(BigInteger.ONE) < 0) {
                throw new RuntimeException("Invalid quantity");
            }
            if(request.getUnitPrice().compareTo(BigInteger.ONE) < 0){
                throw new RuntimeException("Invalid price");
            }
            request.setToken(token);
            
            return service.addCarRepairCarPart(request);
    }
}
